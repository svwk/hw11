import React from 'react';
import PropTypes from 'prop-types';

export function LoginForm(props) {
    return(      
        <form>  
            <h3>{props.title}</h3>          
            <table align="center">
                <tbody>
                    <tr>
                        <td align="right"><label>Login: </label> </td>
                        <td><input /></td>
                    </tr>
                    <tr>
                        <td align="right"><label>Password:</label></td>
                        <td> <input type="password" /> </td>
                    </tr>
                </tbody>
            </table>
            <br></br>
            <button  onClick = {OnClickEventHandler(props.url)}>Apply</button>
        </form>        
    )
}

LoginForm.defaultProps = {
    title: 'Login',
    url: 'https://jsonplaceholder.typicode.com/posts/1'
}

LoginForm.propTypes = {
    title: PropTypes.string,
    url: PropTypes.string
}

const OnClickEventHandler = (url) => {
    fetch(url)
   .then(response => response.json())
   .then(json => console.log(json));
}