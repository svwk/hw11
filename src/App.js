import './App.css';
import React from 'react';
import {LoginForm} from './loginForm'

function App() {
  return (
    <div className="App">      
      <h1>Home work 11</h1> 
      <h2>Введение в React</h2>      
      <LoginForm url={process.env.apiUrl}/>
    </div>
  );
}

export default App;
